const aws = require('aws-sdk');
const s3 = new aws.S3({apiVersion: '2006-03-01'});
const fs = require('fs');
const tar = require('tar');
const puppeteer = require('puppeteer');
const config = require('./config');

exports.saveScreenshotToS3 = async(page, s3bucket, filename) => {
    let pathSsTmp = '/tmp/screenshot.png';
  await page.screenshot({path: pathSsTmp});

  /*let buffer = await page.screenshot({encoding: "base64"});*/
    debugLog(`Uploading screenshot 's3://${s3bucket}/${filename}'`);
  const screenshot = await new Promise((resolve, reject) => {
    fs.readFile(pathSsTmp, (err, data) => {
      if (err) return reject(err);
      resolve(data);
    });
  });
    const s3Params = {
        Bucket: s3bucket,
        Key: filename,
        Body: screenshot
    };
  await s3.putObject(s3Params).promise();
    /*await s3.putObject(s3Params, (err, data) => {
        debugLog("inside callback");
        if (err) {
            debugLog(err);
        } else {
            debugLog("uploading succeeded");
        }
    }).promise();*/
    debugLog("uploading completed");
};

exports.getBrowser = (() => {
  let browser;
  return async () => {
    if (typeof browser === 'undefined' || !(await isBrowserAvailable(browser))) {
      await setupChrome();    
    debugLog('Launch Puppeteer');
    debugLog( config.executablePath );
      browser = await puppeteer.launch({
        headless: true,
        executablePath: config.executablePath,
        args: config.launchOptionForLambda,
        dumpio: !!exports.DEBUG,
        ignoreHTTPSErrors: true
      });
    debugLog( 'Setup after browser lauch' );
    const version = await browser.version();
    debugLog(`Launch chrome: ${version}`);
      //debugLog(async b => `launch done: ${await browser.version()}`);
    }
    return browser;
  };
})();


const isBrowserAvailable = async browser => {
  try {
    await browser.version();
  } catch (e) {
    debugLog(e); // not opened etc.
    return false;
  }
  return true;
};

const setupChrome = async () => {
  if (!(await existsExecutableChrome())) {
    if (await existsLocalChrome()) {
      debugLog('setup local chrome');
      await setupLocalChrome();
    await existsSetupChromePath();
    await existsExecutableChrome();
    } else {
      debugLog('setup s3 chrome');
      await setupS3Chrome();
    }
    debugLog('setup done');
  }
};

const existsLocalChrome = () => {
  return new Promise((resolve, reject) => {
    fs.exists(config.localChromePath, exists => {
    debugLog( 'Setup existsLocalChrome: ' + ( exists ? 'Yes' : 'No' ) );
      debugLog( 'Setup existsExecutableChrome localChromePath: ' + config.localChromePath );
    resolve(exists);
    });
  });
};

const existsExecutableChrome = () => {
  return new Promise((resolve, reject) => {
    fs.exists(config.executablePath, exists => {
    debugLog( 'Setup existsExecutableChrome: ' + ( exists ? 'Yes' : 'No' ) );
    debugLog( 'Setup existsExecutableChrome executablePath: ' + config.executablePath );
      resolve(exists);
    });
  });
};

const existsSetupChromePath = () => {
  return new Promise((resolve, reject) => {
    fs.exists(config.setupChromePath, exists => {
    debugLog( 'Setup existsSetupChromePath: ' + ( exists ? 'Yes' : 'No' ) );
      debugLog( 'Setup existsSetupChromePath setupChromePath: ' + config.setupChromePath );
      resolve(exists);
    });
  });
};

const setupLocalChrome = () => {
  return new Promise((resolve, reject) => {
    fs.createReadStream(config.localChromePath).on('error', err => reject(err)).pipe(tar.x({
      C: config.setupChromePath
    })).on('error', err => reject(err)).on('end', () => resolve());
  });
};

const setupS3Chrome = () => {
  return new Promise((resolve, reject) => {
    const params = {
      Bucket: config.remoteChromeS3Bucket,
      Key: config.remoteChromeS3Key
    };
    s3.getObject(params).createReadStream().on('error', err => reject(err)).pipe(tar.x({
      C: config.setupChromePath
    })).on('error', err => reject(err)).on('end', () => resolve());
  });
};

const debugLog = log => {
  if (config.DEBUG) {
    let message = log;
    if (typeof log === 'function') message = log();
    Promise.resolve(message).then(message => console.log(message));
  }
};
