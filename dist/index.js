const setup = require('./starter-kit/setup');
const urlParser = require('url');

exports.handler = async function (event, context, callback) {
  // For keeping the browser launch
  console.log('hello');
  context.callbackWaitsForEmptyEventLoop = false;
  console.log('exports.handler: before call getBrowser');
  const browser = await setup.getBrowser();

  console.log('exports.handler: after call getBrowser');
  await exports.run(event.url, browser, event.trackResponses, event.getCookies).then(result => callback(null, result)).catch(err => callback(err));
  console.log('exports.handler: after exports.run');
};

exports.run = async function (url, browser, trackResponses = false, getCookies = false) {
  console.log('exports.run: begin run');
  console.log('exports.run: url: ' + url);

  const page = await browser.newPage();

  //If you have error denied here because the role you use don't
  //have full access to lambda
  console.log(typeof page);

  let responses = {};
  if (trackResponses) {
    const parsedUrl = urlParser.parse(url);
    page.on('response', async function (response) {
      // make sure only tracks response from same host
      if (response.url.indexOf(parsedUrl.hostname) !== -1) {
        responses[response.url] = await response.text();
      }
    });
  }
  const override = Object.assign(page.viewport(), { width: 1024 });
  await page.setViewport(override);
  console.log('exports.run: after browser.newPage');
  await page.goto(url, { waitUntil: ['domcontentloaded', 'networkidle0'] });
  const html = await page.content();
  const cookies = getCookies ? await page.cookies() : '';

  //console.log( 'exports.run: after page.goto' );
  await setup.saveScreenshotToS3(page, 'snapshot.puppeteer.incart.co', 'test.png');
  //console.log( 'exports.run: after setup.saveScreenshotToS3' );  

  await page.close();
  await browser.close();
  console.log('exports.run: end run');
  return { html, responses, cookies };
};